# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpipewire
pkgver=5.27.0
pkgrel=0
pkgdesc="Components relating to pipewire use in Plasma"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.1-only AND LGPL-3.0-only AND LicenseRef-KDE-Accepted-LGPL"
depends="
	pipewire
	"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kcoreaddons-dev
	ki18n-dev
	kwayland-dev
	libdrm-dev
	libepoxy-dev
	pipewire-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	samurai
	wayland-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/$_rel/plasma/$pkgver/kpipewire-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
eb01a3a4c77c144ccf6f7f0b1ab268dc6d3106e98aee8d512ac7b8f6e0def31afd3bf3fa48200522bc62a6e8c97f171a5ddcddc3faca5f758a2a0c98dc1db3a3  kpipewire-5.27.0.tar.xz
"

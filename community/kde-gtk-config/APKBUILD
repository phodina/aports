# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-gtk-config
pkgver=5.27.0
pkgrel=0
pkgdesc="GTK2 and GTK3 Configurator for KDE"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-gtk-config"
license="GPL-2.0 AND LGPL-2.1-only OR LGPL-3.0-only"
depends="gsettings-desktop-schemas"
makedepends="
	extra-cmake-modules
	gsettings-desktop-schemas-dev
	gtk+2.0-dev
	gtk+3.0-dev
	karchive-dev
	kcmutils-dev
	kconfigwidgets-dev
	kdecoration-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
565e06a0597ab474cbcf927d55839f93aa1d06cf53730395b27900652d9a1bcb58af2236bd77c507b82626bfab06a1c8004a4afa095e42af51e97619cda172dd  kde-gtk-config-5.27.0.tar.xz
"

# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=6.13.0
pkgrel=0
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	ansible-core
	git
	py3-ansible-compat
	py3-filelock
	py3-jinja2
	py3-jsonschema
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-wcmatch
	py3-yaml
	python3
	yamllint
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
source="ansible-lint-$pkgver.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver/ansible-lint-v$pkgver.tar.gz"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
1ce48c2a71e873fcdb7bc37b6d0ad3c231e0512199d276b43d3f1cccb68ac36b5f00910f16f95efb4bff2c3fe0721302cad6e2f078a06fb9a3fd8a41e114e165  ansible-lint-6.13.0.tar.gz
"

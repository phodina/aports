# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=polkit-kde-agent-1
pkgver=5.27.0
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="polkit-elogind"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/polkit-kde-agent-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
1fec93babfc63246686f5db6823e7d7e7d2c21c366e14f0edc85d74583ff0989d5a26de1d81ea291d166dbbf4daa5ce8bff53b4af1526c8632843de09c9f7b0a  polkit-kde-agent-1-5.27.0.tar.xz
"
